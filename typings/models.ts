import { ModelCtor } from 'sequelize'
import { AddressModelCtor } from '@models/address-model'

export interface listImportModel {
  default: ModelCtor<any>;
  associate?: any;
}

export interface clearModel {
  model: keyof ListModels;
  where?: { [key: string]: any;};
}

export interface ListModels {
  AddressModel: AddressModelCtor
}
