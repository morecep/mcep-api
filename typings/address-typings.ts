import { FindOptions, WhereOptions } from 'sequelize'
import { AddressAttributes } from '@models/address-model'

export interface WhereAddressData {
  where?: WhereOptions<AddressAttributes>;
  options?: FindOptions<AddressAttributes>;
}

export interface CreateAddressData {
  cep: string;
  street: string;
  complement: string;
  neighborhood: string;
  city: string;
  state: string;
  ibge: string;
  gia: string;
  ddd: string;
  siafi: string;
}

export interface DefaultAddressData {
  cep: string;
  logradouro: string;
  complemento: string;
  bairro: string;
  localidade: string;
  uf: string;
  ibge: string;
  gia: string;
  ddd: string;
  siafi: string;
}
