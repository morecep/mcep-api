# Como iniciar o projeto

Antes de tudo, devemos ter um banco de dados em **POSTGRES** e criar uma base com um nome qualquer que será usado nas variáveis de ambiente. (ex: `mcpe-api-dev`) 

Primeiro devemos instalar as dependências do projeto, e para isso devemos executar o comando
`yarn` pois alguns script dependem do yarn para serem executados. **(A aplicação funciona somente com `yarn`)**

Agora vamos criar o nosso arquivo de variáveis de ambiente. Temos que criar um arquivo com o nome `.env`
na raiz do projeto e copiar os dados do arquivo `.env.example` para o nosso arquivo que acabamos de criar (.env).

Após criar o arquivo `.env` e copiar o dados a ele, devemos editar os valores das seguinte variáveis;
- **NODE_ENV**: corresponde ao ambiente que o projeto irá ser iniciado: `develoment` ou `production` (ex: `development`);
- **POSTGRES_DATABASE**: deverá ser posto o nome da base criada no postgres; (ex: `mcep-api-dev`)
- **POSTGRES_HOST**: deverá ser posto a url do postgres; (ex: `localhost`)
- **POSTGRES_USER**: deverá ser posto o usuário do postgres; (ex: `postgres`)
- **POSTGRES_PASSWORD**: deverá ser posto a senha do postgres; (ex: `postgres`)
- **POSTGRES_PORT**: deverá ser posto a porta do postgres; (ex: `5432`)

Após o setup das variáveis de ambiente, tempo que executar o comando `yarn base:init` para rodar as migrações da base de dados
e logo em seguida executar `yarn start:dev` 

Agora é só aguardar o servidor iniciar com sucesso, que irá aparecer a seguinte mensagem: `Servidor iniciado na porta: 3001`


## Para executar os testes

Tendo em vista que as dependências estão instaladas, basta executar o comando `yarn test` para iniciar os testes.
