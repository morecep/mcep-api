import { CreateAddressData } from '@typings/address-typings'
import faker from 'faker'

class AddressBuilder {
  private readonly address: Partial<CreateAddressData>;

  constructor () {
    this.address = {
      cep: faker.random.number({ min: 9, max: 9 }).toString(),
      street: faker.lorem.words(2),
      complement: faker.lorem.words(2),
      state: faker.lorem.word(2),
      city: faker.lorem.word(),
      neighborhood: faker.lorem.word(),
      ddd: faker.random.number(2).toString(),
      gia: faker.random.number(2).toString(),
      siafi: faker.random.number(4).toString(),
      ibge: faker.random.number(6).toString()
    }
  }

  removeCEP (): AddressBuilder {
    this.address.cep = undefined
    return this
  }

  invalidState (): AddressBuilder {
    this.address.state = 'INVALID'
    return this
  }

  build (): Partial<CreateAddressData> {
    return this.address
  }
}

export default AddressBuilder
