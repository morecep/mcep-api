import AddressModel from '@models/address-model'
import Api from '../utils/api'
import AddressBuilder from '../builder/address-builder'

const api = new Api()

beforeAll(async () => {
  await api.init()
  await api.reset()
})

describe('Test actions in table addresses', () => {
  it('Should create a new address in table', async () => {
    const addressData = new AddressBuilder().build()
    const address = await AddressModel.create(<any> addressData)

    expect(address.toJSON()).toEqual({
      ...addressData,
      createdAt: expect.any(Date)
    })
  })

  it("Shouldn't create a address with same cep", async () => {
    const addressData = new AddressBuilder().build()
    await expect(AddressModel.create(<any> addressData)).rejects.toThrow()
  })

  it("Shouldn't create a address with state more than 2 characters", async () => {
    const addressData = new AddressBuilder().invalidState().build()
    await expect(AddressModel.create(<any> addressData)).rejects.toThrow()
  })

  it("Shouldn't create a address without cep", async () => {
    const addressData = new AddressBuilder().removeCEP().build()
    await expect(AddressModel.create(<any> addressData)).rejects.toThrow()
  })
})
