import App from '@src/app'
import models from '@models/.'
import request from 'supertest'
import { clearModel } from '@typings/models'
import { Express } from 'express'

export default class Api {
  server = new App()
  app: Express | undefined;

  constructor (
    private token = '',
    private refreshToken = ''
  ) {
    this.setTokenAndRefreshToken(token, refreshToken)
  }

  async init (): Promise<void> {
    await this.server.init()
    this.app = this.server.getApp()
  }

  setTokenAndRefreshToken (token: string, refreshToken: string): void {
    this.token = token
    this.refreshToken = refreshToken
  }

  get (url: string): request.Test {
    return request(this.app).get(url)
  }

  async reset (): Promise<void> {
    const listModelReset: clearModel[] = [ // Precisa está em ordem para apagar a base
      { model: 'AddressModel' }
    ]

    await models.clearModels(listModelReset)
  }
}
