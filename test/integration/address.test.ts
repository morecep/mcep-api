import Api from '../utils/api'

const api = new Api()

beforeAll(async () => {
  await api.init()
  await api.reset()
})

describe('Test of controller addresses.ts', () => {
  it('Should get cep', async () => {
    const res = await api.get('/addresses/66083106')

    expect(res.body).toEqual({
      status: '200 - OK',
      msg: 'Requisição concluida.',
      code: 'requestDone',
      body: {
        cep: '66083-106',
        logradouro: 'Travessa Alferes Costa',
        complemento: 'de 676/677 a 938/939',
        bairro: 'Pedreira',
        localidade: 'Belém',
        uf: 'PA',
        ibge: '1501402',
        gia: '',
        ddd: '91',
        siafi: '0427'
      }
    })
  })

  it("Shouldn't get cep", async () => {
    const res = await api.get('/addresses/66083104')

    expect(res.body).toEqual({
      status: '400 - Bad Request',
      code: 'viaCEPNotFound',
      msg: 'CEP não encontrado no ViaCEP.'
    })
  })
})
