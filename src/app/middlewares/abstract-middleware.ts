import { Response } from 'express'
import result from '../utils/result'
import { ResultData } from '@typings/result'
import { setHttpContext, getHttpContext } from '@utils/http-context'

abstract class AbstractMiddleware {
  protected setContext = setHttpContext
  protected getContext = getHttpContext

  protected result (res: Response, data: ResultData): void {
    result(res, data)
  }
}

export default AbstractMiddleware
