import { Request, Response, NextFunction } from 'express'
import DatabaseService from '@services/database-service'
import AbstractMiddleware from '@src/app/middlewares/abstract-middleware'
import { logger } from '@logger'
import randomBytes from 'random-bytes'
import { setHttpContext } from '@utils/http-context'
import { ApiError } from '@src/app/exceptions/api-error'

class PreConfig extends AbstractMiddleware {
  async init (req: Request, res: Response, next: NextFunction): Promise<unknown> {
    setHttpContext('req', {
      host: `${req.protocol}://${req.get('host')}`,
      method: req.method || '-',
      originalUrl: req.originalUrl || '-',
      requestId: randomBytes.sync(12).toString('hex') || '-'
    })
    try {
      await DatabaseService.checkConnection()
    } catch (e) {
      logger.error(`Error na conexão com banco de dados: ${e}`)
      throw new ApiError('Internal Server Error', 'connectionDatabaseError')
    }

    return next()
  }
}

export default new PreConfig()
