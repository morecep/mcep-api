import { DataTypes, Model, Optional, ModelCtor } from 'sequelize'
import DatabaseService from '@services/database-service'

const sequelize = DatabaseService.getConnection()

export interface AddressAttributes {
  cep: string;
  street: string;
  complement: string;
  neighborhood: string;
  city: string;
  state: string;
  ibge: string;
  gia: string;
  ddd: string;
  siafi: string;
  createdAt: Date;
}

export type AddressCreation = Optional<AddressAttributes, 'createdAt'>

export interface AddressInstance extends Model<AddressAttributes, AddressCreation>, AddressAttributes {}

export type AddressModelCtor = ModelCtor<AddressInstance>

const AddressModel = sequelize.define<AddressInstance>('addresses', {
  cep: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  street: DataTypes.STRING,
  complement: DataTypes.STRING,
  neighborhood: DataTypes.STRING,
  city: DataTypes.STRING,
  state: DataTypes.STRING(2),
  ibge: DataTypes.STRING,
  gia: DataTypes.STRING,
  ddd: DataTypes.STRING,
  siafi: DataTypes.STRING,
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at',
    get () {
      return new Date(this.getDataValue('createdAt'))
    }
  }
}, {
  updatedAt: false
})

export default AddressModel
