import * as zod from 'zod'
import AbstractController from '@src/app/controllers/abstract-controller'
import { Request, Response } from 'express'
import AddressService from '@services/address-service'

class AddressesController extends AbstractController {
  private addressSrv = new AddressService()

  async getAddress (req: Request, res: Response) {
    const schemaGetAddress = zod.object({
      cep: zod.string().length(8).regex(/\d{8}/g)
    })

    const { cep } = schemaGetAddress.parse({
      cep: req.params.cep
    })

    const cepData = await this.addressSrv.getAddressByCEP(cep)

    return this.result(res, {
      status: 'OK',
      msg: 'requestDone',
      body: cepData
    })
  }
}

export default new AddressesController()
