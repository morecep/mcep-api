import { RouterConfig } from './index'

export default <RouterConfig> {
  id: 'cep',
  routes: [
    { method: 'get', path: '/{id}', function: 'getAddress' }
  ]
}
