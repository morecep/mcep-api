import { Express, Router as RouterExpress } from 'express'
import path from 'path'
import { readdirSync } from 'fs'
import { pathControllers } from '@src/config/paths'
import TimeMiddleware from '../middlewares/time-middleware'
import PreConfig from '../middlewares/pre-config'
import ErrorHandler from '../middlewares/error-handler'
import { logger } from '@logger'
import { ApiError } from '@src/app/exceptions/api-error'

export interface RouterConfig {
  id: string;
  routes: {
    method: 'get' | 'post' | 'delete' | 'put';
    path: string;
    function: string;
  }[];
}

interface RouterList {
  [key: string]: RouterConfig
}

class Router {
  private app: Express
  private routerName: string[] = []
  private routerList: RouterList = {}

  constructor (app: Express) {
    this.app = app
  }

  async init (): Promise<void> {
    logger.info('Iniciando Rotas')
    this.startMiddleware()
    await this.startRouter()
    this.endMiddleware()
  }

  private startMiddleware (): void {
    this.app.use(TimeMiddleware.setTime)
  }

  private endMiddleware (): void {
    this.app.use(ErrorHandler.getError)
    this.app.use('*', (req, res) => {
      logger.error('Rota não encontrada')
      res.status(404).json({
        status: '404 - Not Found',
        msg: 'Route not found'
      })
    })
  }

  private async startRouter (): Promise<void> {
    readdirSync(path.join(__dirname))
      .filter(fileName =>
        (!fileName.includes('index'))
      )
      .forEach(fileName => {
        this.routerName.push(path.basename(Router.getNameRouter(fileName)))
      })

    for (const fileName of this.routerName) {
      const routerName = `${fileName}-router`
      this.routerList[fileName] = (await import(path.join(__dirname, routerName))).default
    }

    for (const router of Object.keys(this.routerList)) {
      logger.info(`===================  ${router.toUpperCase()}  ===================`)
      const routeExpress = RouterExpress()
      const currentRouter = this.routerList[router]
      for (const routerParam of currentRouter.routes) {
        const method = routerParam.method
        const pathName = `/${router + routerParam.path.replace('{id}', ':' + currentRouter.id)}`
        const functionName = routerParam.function
        const controllerName = `${router}-controller`

        const classFunction = (await import(path.join(__dirname, pathControllers, controllerName))).default
        const preConfigClass = PreConfig.init.bind(PreConfig)

        if (!classFunction[functionName]) {
          const msg = `Método ${functionName} não encontrado no controlador ${router}`
          logger.error(msg)
          throw new ApiError('Internal Server Error', 'methodNotFound')
        }

        const listFunction = [
          { name: 'PreConfig', class: preConfigClass },
          { name: functionName, class: classFunction[functionName].bind(classFunction) }
        ]

        routeExpress[method](pathName, ...listFunction.map(item => item.class))
        logger.info(`${method.toUpperCase()} - [ ${functionName} ] ${pathName} ::: ${listFunction.map(item => item.name).join(' => ')}`)
      }
      logger.info('')
      logger.info('')
      this.app.use(routeExpress)
    }
  }

  private static getNameRouter (router: string, separator = '-'): string {
    const splitName = router.split(separator)
    splitName.pop()
    return splitName.join(separator)
  }
}

export default Router
