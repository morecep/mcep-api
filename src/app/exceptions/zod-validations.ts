import AbstractException from './abstract-exception'
import { logger } from '@logger'
import { ResultData } from '@typings/result'
import { ZodError } from 'zod'
import { exceptions } from '@utils/translate'

class ZodValidations extends AbstractException<ZodError> {
  exception (error: ZodError): ResultData {
    logger.error(`Error na validação do zod: ${JSON.stringify(error)}`)
    const allErrors = error.issues.map(err => {
      const replaceFields = []
      let codeException: exceptions | undefined
      switch (err.code) {
        case 'invalid_type': {
          codeException = 'invalidType'
          replaceFields.push(err.expected)
          break
        }

        case 'too_big': {
          codeException = 'maxNumber'
          replaceFields.push(err.maximum.toString())
          break
        }

        case 'invalid_string': {
          codeException = 'invalidString'
          break
        }

        case 'too_small': {
          codeException = 'minNumber'
          replaceFields.push(err.minimum.toString())
          break
        }
      }
      return {
        field: err.path.join('.'),
        error: codeException ? this.replaceText(codeException, replaceFields) : err.message
      }
    })

    return {
      status: 'Unprocessable Entity',
      msg: 'validationError',
      body: allErrors
    }
  }

  private replaceText (exceptionType: exceptions, values: string[]): string {
    let text = exceptions[exceptionType]
    values.map(rf => {
      text = text.replace('%', rf)
    })
    return text
  }
}

export default ZodValidations
