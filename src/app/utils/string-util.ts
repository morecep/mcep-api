export const toCamelCase = (word: string, separator = '-'): string => {
  const splitName = word.split(separator)
  const firstName = splitName[0]
  splitName.shift()
  let wordCapitalize = ''
  for (const name of splitName) {
    wordCapitalize += name.charAt(0).toUpperCase() + name.substr(1)
  }

  return firstName + wordCapitalize
}

export const toPascalCase = (word: string, separator = '-'): string => {
  const nameModelSplit = word.split(separator)
  let wordCapitalize = ''
  for (const modelName of nameModelSplit) {
    wordCapitalize += modelName.charAt(0).toUpperCase() + modelName.substr(1)
  }
  return wordCapitalize
}

export const capitalize = (word: string): string => {
  return word.charAt(0).toUpperCase() + word.substr(1)
}
