export const messages = {
  badRequest: 'Erro no requisição.',
  validationError: 'Erro na validação.',
  connectionDatabaseError: 'Erro na conexão com o banco',
  unknownError: 'Um erro desconhecido ocorreu',
  requestDone: 'Requisição concluida.',
  methodNotFound: 'Método não encontrado.',
  // ==========================================================================================
  viaCEPGeneralError: 'Um error ocorreu ao buscar cep.',
  viaCEPNotFound: 'CEP não encontrado no ViaCEP.',
  // ==========================================================================================
  cepCreatedError: 'Error ao registrar novo cep.',
  cepAlreadyExist: 'O CEP já está registrado.'
}

export const exceptions = {
  minNumber: 'Deve ser maior ou igual a % caracter(s).',
  maxNumber: 'Deve ser menor ou igual a % caracter(s).',
  invalidType: 'Não é do tipo %.',
  invalidString: 'Valor não compreendido ou mal formatado.'
}

export type messages = keyof typeof messages
export type exceptions = keyof typeof exceptions
