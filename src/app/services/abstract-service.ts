import constants from '@src/config/constants'
import { setHttpContext, getHttpContext } from '@utils/http-context'

abstract class AbstractService {
  protected Constants = constants
  protected setContext = setHttpContext
  protected getContext = getHttpContext
}

export default AbstractService
