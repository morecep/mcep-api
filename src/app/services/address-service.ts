import AbstractService from '@services/abstract-service'
import { logger } from '@logger'
import AddressModel, { AddressInstance } from '@models/address-model'
import { CreateAddressData, DefaultAddressData, WhereAddressData } from '@typings/address-typings'
import ViaCEPService from '@services/viacep-service'
import { ApiError } from '@src/app/exceptions/api-error'

class AddressService extends AbstractService {
  private viaCEPSrv = new ViaCEPService()

  async findAll (data?: WhereAddressData): Promise<AddressInstance[]> {
    return await AddressModel.findAll({
      where: data && data.where,
      ...(data && data.options)
    })
  }

  async findOneByCEP (cep: string): Promise<AddressInstance | null> {
    const listAddress = await this.findAll({
      where: {
        cep
      }
    })

    return listAddress.length ? listAddress[0] : null
  }

  async getAddressByCEP (cep: string): Promise<DefaultAddressData> {
    logger.info(`Buscando na base pelo cep: ${cep}`)
    const cepInDB = await this.findOneByCEP(this.formatCEP(cep))

    if (cepInDB) {
      logger.info('CEP encontrado na base')
      logger.debug(`DATA: ${JSON.stringify(cepInDB.toJSON())}`)
      return this.MapDBtoDefaultCEP(cepInDB)
    }

    logger.info('CEP não encontrado na base, buscando pelo WB viaCEP')
    const cepData = await this.viaCEPSrv.getAddressByCEP(cep)

    if (!cepData) {
      throw new ApiError('Bad Request', 'viaCEPNotFound')
    }

    await this.createAddress({
      cep: cepData.cep,
      street: cepData.logradouro,
      city: cepData.localidade,
      complement: cepData.complemento,
      ddd: cepData.ddd,
      gia: cepData.gia,
      ibge: cepData.ibge,
      siafi: cepData.siafi,
      state: cepData.uf,
      neighborhood: cepData.bairro
    })

    return cepData
  }

  private async createAddress (data: CreateAddressData): Promise<void> {
    logger.info('Registrando um novo endereço')
    logger.debug(`DATA: ${JSON.stringify(data)}`)

    logger.info(`Buscando se exite o cep: ${data.cep}`)
    const existCEP = await this.findOneByCEP(this.formatCEP(data.cep))

    if (existCEP) {
      logger.error('Já existe um cep cadastrado')
      throw new ApiError('Bad Request', 'cepAlreadyExist')
    }

    try {
      logger.info('Registrando informações...')
      await AddressModel.create(data)

      logger.info('CEP registrado com sucesso.')
    } catch (e) {
      logger.error(`Error ao registrar o cep: ${e}`)
      throw new ApiError('Internal Server Error', 'cepCreatedError')
    }
  }

  private MapDBtoDefaultCEP (address: AddressInstance): DefaultAddressData {
    return this.MapObject<DefaultAddressData>(<Record<string, string>>address.toJSON(), [
      ['cep', 'cep'],
      ['street', 'logradouro'],
      ['complement', 'complemento'],
      ['neighborhood', 'bairro'],
      ['city', 'localidade'],
      ['state', 'uf'],
      ['ibge', 'ibge'],
      ['gia', 'gia'],
      ['ddd', 'ddd'],
      ['siafi', 'siafi']
    ])
  }

  private MapObject <T> (obj: Record<string, unknown>, map: [string, string][]): T {
    let mapObj: any = {}
    map.map(item => {
      const keyLeft = item[0]
      const keyRight = item[1]
      mapObj = {
        ...mapObj,
        [keyRight]: obj[keyLeft]
      }
    })
    return mapObj
  }

  private formatCEP (cep: string): string {
    const formatCEP = cep.substring(0, 5) + '-' + cep.substring(5)
    logger.info(`CEP formatado: ${formatCEP}`)
    return formatCEP
  }
}

export default AddressService
