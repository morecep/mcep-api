import { DefaultAddressData } from '@typings/address-typings'
import AxiosService from '@services/axios-service'
import { logger } from '@logger'
import { ApiError } from '@src/app/exceptions/api-error'

class ViaCEPService {
  private axiosSrv = new AxiosService().getAxios()

  async getAddressByCEP (cep: string): Promise<DefaultAddressData | null> {
    logger.info(`Buscando o cep [ ${cep} ] no ViaCEP`)
    const cepFormat = cep.split('-').join('')

    try {
      const { data } = await this.axiosSrv.get(`/${cepFormat}/json`)

      logger.debug(`DATA VIACEP: ${JSON.stringify(data)}`)
      if (data.erro === undefined) {
        logger.info('CEP encontrado no ViaCEP')
        return data
      }
      logger.error('CEP não encontrado no ViaCEP')
      return null
    } catch (e) {
      logger.error(`Um error ocorreu ao buscar cep no viaCEP: ${e}`)
      throw new ApiError('Bad Request', 'viaCEPGeneralError')
    }
  }
}

export default ViaCEPService
