import axios, { AxiosInstance } from 'axios'

class AxiosService {
  private readonly axiosInstance: AxiosInstance
  constructor () {
    this.axiosInstance = axios.create({
      baseURL: 'ttps://viacep.com.br/ws/'
    })
  }

  getAxios (): AxiosInstance {
    return this.axiosInstance
  }
}

export default AxiosService
