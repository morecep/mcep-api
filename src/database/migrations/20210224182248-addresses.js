'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('addresses', {
      cep: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      street: {
        type: Sequelize.STRING,
        allowNull: false
      },
      complement: {
        type: Sequelize.STRING,
        allowNull: false
      },
      neighborhood: {
        type: Sequelize.STRING,
        allowNull: false
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state: {
        type: Sequelize.STRING(2),
        allowNull: false
      },
      ibge: {
        type: Sequelize.STRING,
        allowNull: false
      },
      gia: {
        type: Sequelize.STRING,
        allowNull: false
      },
      ddd: {
        type: Sequelize.STRING,
        allowNull: false
      },
      siafi: {
        type: Sequelize.STRING,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('addresses')
  }
}
